
# Copyright 2014-2019 The ODL contributors
#
# This file is part of ODL.
#
# This Source Code Form is subject to the terms of the Mozilla Public License,
# v. 2.0. If a copy of the MPL was not distributed with this file, You can
# obtain one at https://mozilla.org/MPL/2.0/.

"""Maximum Likelihood Expectation Maximization algorithm."""

from __future__ import print_function, division, absolute_import
import numpy as np

__all__ = ('mlem', 'osmlem', 'poisson_log_likelihood')

def mlem(op, x, data, niter, noise='poisson', callback=None, **kwargs):
    """Maximum Likelihood Expectation Maximation algorithm.

    Attempts to solve::

        max_x L(x | data)

    where ``L(x | data)`` is the Poisson likelihood of ``x`` given ``data``.
    The likelihood depends on the forward operator ``op`` such that
    (approximately)::

        op(x) = data


    Parameters
    ----------
    op : `Operator`
        Forward operator in the inverse problem.
    x : ``op.domain`` element
        Vector to which the result is written. Its initial value is
        used as starting point of the iteration, and its values are
        updated in each iteration step.
        The initial value of ``x`` should be non-negative.
    data : ``op.range`` `element-like`
        Right-hand side of the equation defining the inverse problem.
    niter : int
        Number of iterations.
    callback : callable, optional
        Function called with the current iterate after each iteration.

    Other Parameters
    ----------------
    sensitivities : float or ``op.domain`` `element-like`, optional
        The algorithm contains a ``A^T 1``
        term, if this parameter is given, it is replaced by it.
        Default: ``op.adjoint(op.range.one())``

    Notes
    -----
    Given a forward model :math:`A` and data :math:`g`,
    the algorithm attempts to find an :math:`x` that maximizes:

    .. math::
        P(g | g \text{ is } Poisson(A(x)) \text{ distributed}).

    The algorithm is explicitly given by:

    .. math::
       x_{n+1} = \frac{x_n}{A^* 1} A^* (g / A(x_n))

    See Also
    --------
    osmlem : Ordered subsets MLEM
    loglikelihood : Function for calculating the logarithm of the likelihood
    """
    osmlem([op], x, [data], niter=niter, callback=callback,
           **kwargs)


def osmlem(op, x, data, niter, callback=None, **kwargs):
    r"""Ordered Subsets Maximum Likelihood Expectation Maximation algorithm.

    This solver attempts to solve::

        max_x L(x | data)

    where ``L(x, | data)`` is the likelihood of ``x`` given ``data``. The
    likelihood depends on the forward operators ``op[0], ..., op[n-1]`` such
    that (approximately)::

        op[i](x) = data[i]


    Parameters
    ----------
    op : sequence of `Operator`
        Forward operators in the inverse problem.
    x : ``op.domain`` element
        Vector to which the result is written. Its initial value is
        used as starting point of the iteration, and its values are
        updated in each iteration step.
        The initial value of ``x`` should be non-negative.
      data : sequence of ``op.range`` `element-like`
        Right-hand sides of the equation defining the inverse problem.
    niter : int
        Number of iterations.
    callback : callable, optional
        Function called with the current iterate after each iteration.

    Other Parameters
    ----------------
    sensitivities : float or ``op.domain`` `element-like`, optional
        The algorithm contains an ``A^T 1``
        term, if this parameter is given, it is replaced by it.
        Default: ``op[i].adjoint(op[i].range.one())``

    Notes
    -----
    Given forward models :math:`A_i`, and data :math:`g_i`,
    :math:`i = 1, ..., M`,
    the algorithm attempts to find an :math:`x` that
    maximizes:

    .. math::
        \prod_{i=1}^M P(g_i | g_i \text{ is }
        Poisson(A_i(x)) \text{ distributed}).

    The algorithm is explicitly given by partial updates:

    .. math::
       x_{n + m/M} =
       \frac{x_{n + (m - 1)/M}}{A_i^* 1} A_i^* (g_i / A_i(x_{n + (m - 1)/M}))

    for :math:`m = 1, ..., M` and :math:`x_{n+1} = x_{n + M/M}`.

    The algorithm is not guaranteed to converge, but works for many practical
    problems.

    References
    ----------
    Natterer, F. Mathematical Methods in Image Reconstruction, section 5.3.2.

    See Also
    --------
    mlem : Ordinary MLEM algorithm without subsets.
    loglikelihood : Function for calculating the logarithm of the likelihood
    """
    n_ops = len(op)
    if len(data) != n_ops:
        raise ValueError('number of data ({}) does not match number of '
                         'operators ({})'.format(len(data), n_ops))
    if not all(x in opi.domain for opi in op):
        raise ValueError('`x` not an element in the domains of all operators')

    # Convert data to range elements
    data = [op[i].range.element(data[i]) for i in range(len(op))]

    # Parameter used to enforce positivity.
    # TODO: let users give this.
    eps = 1e-8

    if np.any(np.less(x, 0)):
        raise ValueError('`x` must be non-negative')

        # Extract the sensitivites parameter
        sensitivities = kwargs.pop('sensitivities', None)
        reg_param = kwargs.pop('reg_param', 0.)
        reg_func = kwargs.pop('reg_func', None)
        line_search = kwargs.pop('line_search', None)
        # Add eps to data

        if op[0].is_linear and line_search is None:

            if sensitivities is None:
                sensitivities = [np.maximum(opi.adjoint(opi.range.one()), eps)
                                 for opi in op]
            else:
                # Make sure the sensitivities is a list of the correct size.
                try:
                    list(sensitivities)
                except TypeError:
                    sensitivities = [sensitivities] * n_ops

            tmp_dom = op[0].domain.element()
            tmp_ran = [opi.range.element() for opi in op]

            for _ in range(niter):
                for i in range(n_ops):
                    op[i](x, out=tmp_ran[i])
                    tmp_ran[i].ufuncs.maximum(eps, out=tmp_ran[i])
                    data[i].divide(tmp_ran[i], out=tmp_ran[i])

                    op[i].adjoint(tmp_ran[i], out=tmp_dom)
                    tmp_dom /= sensitivities[i]

                    x *= tmp_dom

                    if callback is not None:
                        callback(x)
        else:
            deriv_sign = kwargs.pop('deriv_sign', 'pos')
            U = op[0].domain.element()
            tmp_ran = [opi.range.element() for opi in op]

            for _ in range(niter):
                for i in range(n_ops):
                    if deriv_sign == 'pos':
                        V = [np.maximum(opi.derivative(x).adjoint(opi.range.one()), eps)
                             for opi in op]
                    elif deriv_sign == 'neg':
                        V = [np.minimum(opi.derivative(x).adjoint(opi.range.one()), -eps)
                             for opi in op]
                    else:
                        raise ValueError('`deriv_sign` must be `pos` or `neg`')

                    op[i](x, out=tmp_ran[i])
                    tmp_ran[i].ufuncs.maximum(eps, out=tmp_ran[i])
                    data[i].divide(tmp_ran[i], out=tmp_ran[i])
                    op[i].derivative(x).adjoint(tmp_ran[i], out=U)
                    if deriv_sign == 'pos':
                        U = np.maximum(U, eps)
                    elif deriv_sign == 'neg':
                        U = np.minimum(U, -eps)
                    if deriv_sign == 'neg':
                        if reg_func is not None:
                            V = V + reg_param*reg_func.gradient(x)
                        jac = V[i] - U
                        D = - x/U
                        LS = V[i] / U
                    else:
                        if reg_func is not None:
                            V[i] = V[i] + reg_param*reg_func.gradient(x)
                        jac = V[i] - U
                        D = x/V[i]
                        LS = U / V[i]

                    if line_search is not None:
                        # find indices with Vk/Uk<1
                        negVU = np.where(LS.asarray() < 1.)

                    # find index with minimum and calculate bound aub
                        VUmin = np.amin(LS.asarray()[negVU])
                        aub = 1/(1-VUmin)
                    # perform line search in alpha=[a,aub] by Armijo rule
                   # line_search = odl.solvers.util.steplen.BacktrackingLineSearch(callback, alpha = aub)
                        line_search.alpha = aub

                        step_len = line_search(x, -jac*D)
                    else:
                        step_len = 1.0
                    x += step_len * (-jac*D)

                    if callback is not None:
                        callback(x)

    else:
        raise RuntimeError('unknown noise model')


def poisson_log_likelihood(x, data):
    """Poisson log-likelihood of ``data`` given noise parametrized by ``x``.

    Parameters
    ----------
    x : ``op.domain`` element
        Value to condition the log-likelihood on.
    data : ``op.range`` element
        Data whose log-likelihood given ``x`` shall be calculated.
    """
    if np.any(np.less(x, 0)):
        raise ValueError('`x` must be non-negative')

    return np.sum(data * np.log(x + 1e-8) - x)

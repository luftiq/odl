# Copyright 2014-2019 The ODL contributors
#
# This file is part of ODL.
#
# This Source Code Form is subject to the terms of the Mozilla Public License,
# v. 2.0. If a copy of the MPL was not distributed with this file, You can
# obtain one at https://mozilla.org/MPL/2.0/.

"""Bregman algorithm."""



from __future__ import print_function, division, absolute_import
import numpy as np

from odl.operator import TranslationOperator, OperatorSum, IdentityOperator, ConstantOperator, BroadcastOperator
from odl.solvers.nonsmooth.primal_dual_hybrid_gradient import pdhg, pdhg_stepsize
from odl.solvers.util.callback import CallbackApply, CallbackPrintIteration
from odl.solvers import functional
from odl.solvers.nonsmooth.proximal_gradient_solvers import proximal_gradient
from odl.solvers.nonsmooth.forward_backward import forward_backward_pd
from odl.operator import Operator, OpDomainError
from odl.operator import power_method_opnorm
from odl.solvers.nonsmooth.douglas_rachford import douglas_rachford_pd, douglas_rachford_pd_stepsize
from odl.solvers.iterative.iterative import conjugate_gradient
from odl.solvers.util.steplen import BacktrackingLineSearch

__all__ = ('linearized_bregman', 'gn_bregman')


def gn_bregman(x, f, g, Lf, Lg, p, niter, alpha, kappa , tol=None, **kwargs):
    E = f*Lf
    R = g*Lg
    R_grad = R.gradient
    E_grad = E.gradient
    niter_inner=20
    id_op = IdentityOperator(Lf.domain)
    l2normsquared = functional.L2NormSquared(Lf.domain)
    zerof = functional.ZeroFunctional(Lf.domain)
    
    for k in range(niter):
        innerfunc = functional.FunctionalQuadraticPerturb(zerof, 0, constant=0, linear_term = -p)
        obj_func = E + alpha*R + 0.5*alpha*kappa*l2normsquared - alpha*innerfunc
        for i in range(niter_inner):
           
            print(obj_func(x))
            f_hess = f.gradient.derivative(Lf(x))
            J = Lf.derivative(x)
            R_second_deriv = OperatorSum(alpha * R_grad.derivative(x), alpha*kappa*id_op)
            E_hess_approx = (J.adjoint * f_hess * J)
            hessian = OperatorSum(E_hess_approx, R_second_deriv)
            search_direction = x.space.zero()
            temp = J.adjoint
            temp2 = f.gradient(Lf(x))
            deriv_in_point = temp(temp2) + alpha*R.gradient(x) - alpha * p + alpha*kappa*x
            conjugate_gradient(hessian, search_direction,
                                -deriv_in_point, 40)
            line_search = BacktrackingLineSearch(obj_func, tau=0.5, discount=0.01, alpha=1.0, estimate_step=False)
            lam = line_search(x, search_direction)
            #lam=1
            x += lam * search_direction
        temp = (Lf.derivative(x)).adjoint
        temp2 = f.gradient(Lf(x))
        p -= temp(temp2)
        

        # Updating
        x += step_length * search_direction
        
        if callback is not None:
            callback(x)

def linearized_bregman(x, f, g, Lf, Lg, p, niter, tau, tol=None, **kwargs):
    """Specialized Linearized Bregman algorithm
    
    Parameters
    ----------------

    Notes
    -----

    References
    ----------
    

    """
    from numpy import remainder
    from numpy import isinf

    # if not isinstance(Lf, Operator) or not isinstance(Lg, Operator):
    #     raise TypeError('`op` {!r} is not an `Operator` instance'
    #                     ''.format(Lf))

    if x not in Lf.domain:
        raise OpDomainError('`x` {!r} is not in the domain of `op` {!r}'
                            ''.format(x, Lf.domain))

    niter, niter_in = int(niter), niter
    if niter < 0 or niter != niter_in:
        raise ValueError('`niter` must be a non-negative integer, got {}'
                         ''.format(niter_in))

    if tol is None:
        tol = np.inf
        
        
    callback = kwargs.pop('callback', None)
    if callback is not None and not callable(callback):
        raise TypeError('`callback` {} is not callable'.format(callback))
    

    # J is regularization functional g plus a quadratic term \|.\|^2 * kappa/2     
    # need subgradient p0 in subdifferential of J(x0)
    # non-linear operator is L
    # data fidelity is f
    # regularization parameter is lam
    
    
    # # Initialize range variables (quantities in [] are zero initially)
    # y = L.range.zero()
    # # mu = [mu_old +] delta * (L(x) [- y])
    #p = L.domain.zero()
    # # Temporary for Lx
    # tmp_ran = L.range.element()
    # # Temporary for L^*(mubar)
    # tmp_dom = L.domain.element()
    #lip = L.derivative(L.domain.zero())
    xspace = x.space
    E = f * Lf 
    #R = g * Lg
    niter_inner=15
    idop = IdentityOperator(xspace)
   # L_inner=BroadcastOperator(Lg,idop)
   # Lnorm = power_method_opnorm(L_inner,maxiter=100)
   # tau_inner = 0.01
   # sigma_inner=45/Lnorm**2
    L_inner = Lg
    
  #  Lnorm = power_method_opnorm(L, maxiter=100)
   # tau_inner,sigma_inner = pdhg_stepsize(Lnorm)
    tau_inner, sigma_inner = douglas_rachford_pd_stepsize(L_inner)
    for i in range(niter):  
        xold = x.copy()
       # xx = x.copy()
       # xxold = xx.copy()
       # gradE = E.gradient(x)
       # temp = x + tau * (p - gradE)
       # prox = g.proximal(tau)
       # x = prox(temp)
        
        
        # Eval = E(x) 
        # # formulate subproblem
        # #evalx = L(x)
        # # Eval = E(x)
        # gradE = E.gradient(x)
        # breg = tau*R.bregman(x, p)
        # #l2squared = 0.5*odl.solvers.L2NormSquared(xspace).translated(x)
        # constant = -tau*R(x)
        # breg_term = functional.QuadraticForm(vector=-tau*p, constant=constant)
        # breg_term = breg_term.translated(x)
        # quadform = functional.FunctionalQuadraticPerturb(functional.ZeroFunctional(xspace), quadratic_coeff=0.5, linear_term=tau*gradE, constant=0)
        # quadform=quadform.translated(x)
        # f_inner=quadform
        # g_inner=functional.SeparableSum(tau*g,breg_term)
       
        # def print_val(x):
        #     #print(quadform(x) + breg(x))
        #     print(f_inner(x) + g_inner(L_inner(x)))
        # callback_inner = (CallbackPrintIteration(step=5) &
        #      CallbackApply(print_val, step=5))
        # #forward_backward_pd(
        #x=x, f=f_inner, g=[g_inner], L=[L_inner], h=breg_term, tau=tau_inner, sigma=[sigma_inner],
        #niter=niter_inner, callback=callback_inner)
       # pdhg(x, f_inner, g_inner, L_inner, niter_inner, tau=tau_inner, sigma=sigma_inner, callback=callback_inner)
        #now solve problem with quadform + bregman distance
        
        
        
        
        #tmp = x + tau * (p - gradE)
        #prox = g.proximal(tau)
        #x = prox(tmp)
        # Enew = E(x)
        # while Enew > Eval + 1e-10:
        #     x = xold.copy()
        #     tau = tau * 5 / 6
        #     Eval = E(x)
        #     gradE = E.gradient(x)
        #     tmp = x + tau * (p - gradE)
        #     prox = R.proximal(tau)
        #     x = prox(tmp)
        #     Enew = E(x)
        #     print(tau)
        gradE = E.gradient(x)
        #constant = - tau * R(xx)
        #quadratic_term = functional.FunctionalQuadraticPerturb(functional.ZeroFunctional(xspace), quadratic_coeff = 0.5, linear_term = tau*(gradE-p), constant = constant)
        #quadratic_term = quadratic_term.translated(xx)
        #f_inner = quadratic_term
        temp = x + tau * (p - gradE)
       # g_inner = [g]
        l2normsquared = 0.5 * functional.L2NormSquared(xspace) / tau
        l2normsquared = l2normsquared.translated(temp)
      #  f_inner = l2normsquared
        f_inner = g[0]
        g_inner = [l2normsquared, g[1]]
       # derivx = L.derivative(x)
       # datapart = f.translated(-evalx)
        #K = TranslationOperator(xspace, 1, -1, x)
       # K =  OperatorSum(IdentityOperator(xspace), ConstantOperator(-x, domain=xspace, range=xspace))
       # KK = derivx * K
       # bregpart = tau * J.bregman(xold, p)
        # solve subproblem with PDHG  
        def print_obj(x):
            value = f_inner(x) + g_inner[0](L_inner[0](x)) 
            print(value)
        callback_inner = CallbackPrintIteration(step=20) & CallbackApply(print_obj, step=20)
       # pdhg(x, bregpart, datapart, KK, 1500, tau=None, sigma=None, callback=callback_inner)
        douglas_rachford_pd(x, f_inner, g_inner, L_inner, niter_inner, tau=tau_inner, sigma=sigma_inner)
       # tmp = f.gradient(evalx + derivx(x - xold))
       # p = p - (1/alpha) * derivx.adjoint(tmp)
        p -= (x - xold + tau * gradE) / tau
        if callback is not None:
            callback(x)
    return x

if __name__ == '__main__':
    from odl.util.testutils import run_doctests
    run_doctests()

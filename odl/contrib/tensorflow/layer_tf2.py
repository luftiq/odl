"""Utilities for converting ODL operators to tensorflow layers. Tensorflow 2 version"""

from __future__ import print_function, division, absolute_import
import numpy as np
import odl
import tensorflow as tf
import functools

__all__ = ('as_tensorflow_keras_layer',
           'as_tensorflow_keras_layer_adj_deriv', )


def tf_custom_gradient_method_op_layer(f):
    @functools.wraps(f)
    def wrapped(self, *args, **kwargs):
        if not hasattr(self, '_tf_custom_gradient_wrappers'):
            self._tf_custom_gradient_wrappers = {}
        if f not in self._tf_custom_gradient_wrappers:
            self._tf_custom_gradient_wrappers[f] = tf.custom_gradient(
                lambda *a, **kw: f(self, *a, **kw))
        return self._tf_custom_gradient_wrappers[f](*args, **kwargs)
    return wrapped

def as_tensorflow_keras_layer(odl_op, name='ODLOperator', differentiable=True):
    """Convert `Operator` or `Functional` to a tensorflow layer.

    Parameters
    ----------
    odl_op : `Operator` or `Functional`
        The operator that should be wrapped as a tensorflow layer.
    name : str
        Default name for tensorflow layers created.
    differentiable : boolean
        ``True`` if the layer should be differentiable, in which case
        ``odl_op`` should implement `Operator.derivative` which in turn
        implements `Operator.adjoint`. In this case, the adjoint of the
        derivative is properly wrapped in ``tensorflow_layer``, and
        gradients propagate as expected.

        If ``False``, the gradient is defined as everywhere zero.

    Returns
    -------
    tensorflow_layer : callable
        Callable that, when called with a `tensorflow.Tensor` of shape
        ``(n,) + odl_op.domain.shape + (1,)`` where ``n`` is the batch size,
        returns another `tensorflow.Tensor` which is a lazy evaluation of
        ``odl_op``.

        If ``odl_op`` is an `Operator`, the shape of the returned tensor is
        ``(n,) + odl_op.range.shape + (1,)``.

        If ``odl_op`` is an `Functional`, the shape of the returned tensor is
        ``(n,)``.

        The ``dtype`` of the tensor is ``odl_op.range.dtype``.
    """
    if tf.__version__ < "2.0.0":
        print('Requires Tensorflow 2.')
        return

    class Odl_Op_Layer(tf.keras.layers.Layer):
        '''Keras Layer class for representing ODL Operator'''
        def __init__(self):
            super(Odl_Op_Layer, self).__init__()
            self.op_name = name

        def build(self, input_shape):
            # ! When batch size is smaller at the end of an epoch, a shape mismatch is caused.
            try:
                n_x = int(input_shape[0])
                fixed_size = True
            except TypeError:
                n_x = input_shape[0]
                fixed_size = False
            in_shape = (n_x,) + space_shape(odl_op.domain) + (1,)
            if odl_op.is_functional:
                out_shape = (n_x,)
            else:
                out_shape = (n_x,) + space_shape(odl_op.range) + (1,)
            assert input_shape[1:] == space_shape(odl_op.domain) + (1,)
            out_dtype = getattr(odl_op.range, 'dtype', odl_op.domain.dtype)
            self.oshape = out_shape
            self.fixed_size = fixed_size
            self.ishape = in_shape
            self.odtype = out_dtype
            super(Odl_Op_Layer, self).build(input_shape)

#        @tf.custom_gradient
        def _call_impl_nondiff(self, x):
            '''Implementation of call to odl_op'''

            # check input/output space
            if self.fixed_size:
                x_out_shape = self.oshape
                assert x.shape == self.ishape
            else:
                x_out_shape = (x.shape[0],) + self.oshape[1:]
                assert x.shape[1:] == self.ishape[1:]

            # Evaluate the operator on all inputs in the batch.
            out = np.empty(x_out_shape, self.odtype)
            out_element = odl_op.range.element()
            for i in range(x_out_shape[0]):
                if odl_op.is_functional:
                    out[i] = odl_op(x[i, ..., 0])
                else:
                    odl_op(x[i, ..., 0], out=out_element)
                    out[i, ..., 0] = np.asarray(out_element)
            
            # def grad_zero(dy):
            #     return tf.zeros_like(x)
            
            # return out, grad_zero
            
            return out

        @tf_custom_gradient_method_op_layer
        def _call_impl(self, x):
            '''Implementation of call to odl_op'''

            # check input/output space
            if self.fixed_size:
                x_out_shape = self.oshape
                assert x.shape == self.ishape
            else:
                x_out_shape = (x.shape[0],) + self.oshape[1:]
                assert x.shape[1:] == self.ishape[1:]

            # Evaluate the operator on all inputs in the batch.
            out = np.empty(x_out_shape, self.odtype)
            out_element = odl_op.range.element()
            for i in range(x_out_shape[0]):
                if odl_op.is_functional:
                    out[i] = odl_op(x[i, ..., 0])
                else:
                    odl_op(x[i, ..., 0], out=out_element)
                    out[i, ..., 0] = np.asarray(out_element)

            def _call_grad_impl(dy):
                if self.fixed_size:
                    x_out_shape = self.ishape
                    assert dy.shape == self.oshape
                else:
                    x_out_shape = (dy.shape[0],) + self.ishape[1:]
                    assert dy.shape[1:] == self.oshape[1:]

                # Evaluate the operator on all inputs in the batch.
                grad_out = np.empty(x_out_shape, self.odtype)
                out_element = odl_op.domain.element()
                for i in range(x_out_shape[0]):
                    if odl_op.is_functional:
                        xi = x[i, ..., 0]
                        dyi = dy[i]
                        grad_out[i, ..., 0] = np.asarray(
                            odl_op.gradient(xi)) * dyi
                    else:
                        xi = x[i, ..., 0]
                        dyi = dy[i, ..., 0]
                        odl_op.derivative(xi).adjoint(dyi,
                                                      out=out_element)
                        grad_out[i, ..., 0] = np.asarray(out_element)

                    # Rescale the domain/range according to the weighting since
                    # tensorflow does not have weighted spaces.
                try:
                    dom_weight = odl_op.domain.weighting.const
                except AttributeError:
                    dom_weight = 1.0

                try:
                    ran_weight = odl_op.range.weighting.const
                except AttributeError:
                    ran_weight = 1.0

                scale = dom_weight / ran_weight
                grad_out *= scale
                return grad_out

            return out, _call_grad_impl

        def call(self, x=None):
            # if differentiable:
            #     result = self._call_impl(x)
            # else:
            #     result = self._call_impl_nondiff(x)
            if differentiable:
               result = tf.py_function(func=self._call_impl, inp=[x], Tout=[tf.as_dtype(self.odtype)])
            else:
               result = tf.py_function(func=self._call_impl_nondiff, inp=[x], Tout=[tf.as_dtype(self.odtype)])
            result = result[0]
            result = tf.convert_to_tensor(result)
            result.set_shape(self.oshape)
            return result

        def compute_output_shape(self, input_shape):
            # if self.fixed_size:
            ##    out_shape = self.oshape
            # else:
            #    out_shape = (input_shape[0],) + self.oshape[1:]
            return self.oshape

    return Odl_Op_Layer()


def as_tensorflow_keras_layer_adj_deriv(odl_op, name='ODLOperator', differentiable=True):
    """Convert `Operator` or `Functional` to a tensorflow layer.

    Parameters
    ----------
    odl_op : `Operator` or `Functional`
        The operator that should be wrapped as a tensorflow layer.
    name : str
        Default name for tensorflow layers created.
    differentiable : boolean
        ``True`` if the layer should be differentiable, in which case
        ``odl_op`` should implement `Operator.derivative` which in turn
        implements `Operator.adjoint`. In this case, the adjoint of the
        derivative is properly wrapped in ``tensorflow_layer``, and
        gradients propagate as expected.

        If ``False``, the gradient is defined as everywhere zero.

    Returns
    -------
    tensorflow_layer : callable
        Callable that, when called with a `tensorflow.Tensor` of shape
        ``(n,) + odl_op.domain.shape + (1,)`` where ``n`` is the batch size,
        returns another `tensorflow.Tensor` which is a lazy evaluation of
        ``odl_op``.

        If ``odl_op`` is an `Operator`, the shape of the returned tensor is
        ``(n,) + odl_op.range.shape + (1,)``.

        If ``odl_op`` is an `Functional`, the shape of the returned tensor is
        ``(n,)``.

        The ``dtype`` of the tensor is ``odl_op.range.dtype``.
    """
    if tf.__version__ < "2.0.0":
        print('Requires Tensorflow 2.')
        return

    class Odl_Op_Layer(tf.keras.layers.Layer):
        '''Keras Layer class for representing ODL Operator'''

        def __init__(self):
            super(Odl_Op_Layer, self).__init__()
            self.op_name = name

        def build(self, input_shape):
            try:
                n_x = int(input_shape[0])
                fixed_size = True
            except TypeError:
                n_x = input_shape[0]
                fixed_size = False
            # deriv take point in domain
            deriv_point_shape = (n_x,) + space_shape(odl_op.domain) + (1,)
            in_shape = (n_x,) + space_shape(odl_op.range) + \
                (1,)  # adj deriv eval point in range
            if odl_op.is_functional:
                out_shape = (n_x,)
                deriv_out_shape = (n_x,)
            else:
                out_shape = (n_x,) + space_shape(odl_op.domain) + \
                    (1,)  # out is in domain
                deriv_out_shape = (n_x,) + space_shape(odl_op.range) + (1,)
            assert input_shape[1:] == space_shape(odl_op.range) + (1,)
            assert deriv_point_shape[1:] == space_shape(odl_op.domain) + (1,)
            out_dtype = getattr(odl_op.domain, 'dtype', odl_op.range.dtype)
            self.oshape = out_shape
            self.fixed_size = fixed_size
            self.ishape = in_shape
            self.deriv_point_shape = deriv_point_shape
            self.deriv_out_shape = deriv_out_shape
            self.odtype = out_dtype
            super(Odl_Op_Layer, self).build(input_shape)

        def _call_impl_nondiff(self, dy, x):
            '''Implementation of call to odl_op'''
            if self.fixed_size:
                x_out_shape = self.oshape
                assert x.shape == self.deriv_point_shape
                assert dy.shape == self.ishape
            else:
                x_out_shape = (x.shape[0],) + self.oshape[1:]
                assert x.shape[1:] == self.deriv_point_shape[1:]
                assert dy.shape[1:] == self.ishape[1:]

            # Evaluate the operator on all inputs in the batch.
            out = np.empty(x_out_shape, self.odtype)
            out_element = odl_op.domain.element()  # adj will return sth in domain
            for i in range(x_out_shape[0]):
                if odl_op.is_functional:
                    out[i] = odl_op(x[i, ..., 0])
                else:
                    odl_op.derivative(x[i, ..., 0]).adjoint(
                        dy[i, ..., 0], out=out_element)
                    out[i, ..., 0] = np.asarray(out_element)
            return out

        @tf_custom_gradient_method_op_layer
        def _call_impl(self, dy, x):
            '''Implementation of call to odl_op'''
            # check input/output space
            if self.fixed_size:
                x_out_shape = self.oshape
                assert x.shape == self.deriv_point_shape
                assert dy.shape == self.ishape
            else:
                x_out_shape = (x.shape[0],) + self.oshape[1:]
                assert x.shape[1:] == self.deriv_point_shape[1:]
                assert dy.shape[1:] == self.ishape[1:]

            # Evaluate the operator on all inputs in the batch.
            out = np.empty(x_out_shape, self.odtype)
            out_element = odl_op.domain.element()  # adj will return sth in domain
            for i in range(x_out_shape[0]):
                if odl_op.is_functional:
                    out[i] = odl_op(x[i, ..., 0])
                else:
                    odl_op.derivative(x[i, ..., 0]).adjoint(
                        dy[i, ..., 0], out=out_element)
                    out[i, ..., 0] = np.asarray(out_element)

            def _call_grad_impl(dz):
                    # dz is the eval point of the adjoint of the derivative of the (adj of the deriv op at point x)
                    # this comes out as (odl_op.derivative(x).adjoint).derivative(w).adjoint(dz) == odl_op.derivative(x)(dz)
                if self.fixed_size:
                    assert dz.shape == x.shape
                else:
                    assert dz.shape[1:] == x.shape[1:]

                # Evaluate the operator on all inputs in the batch.
                grad_out = np.empty(self.deriv_out_shape, self.odtype)
                out_element = odl_op.range.element()
                for i in range(x_out_shape[0]):
                    if odl_op.is_functional:
                        xi = x[i, ..., 0]
                        dzi = dz[i]
                        grad_out[i, ..., 0] = np.asarray(
                            odl_op.gradient(xi)) * dzi
                    else:
                        xi = x[i, ..., 0]
                        dzi = dz[i, ..., 0]
                        odl_op.derivative(xi)(dzi,
                                              out=out_element)
                        grad_out[i, ..., 0] = np.asarray(out_element)

                # Rescale the domain/range according to the weighting since
                # tensorflow does not have weighted spaces.
                try:
                    dom_weight = odl_op.domain.weighting.const
                except AttributeError:
                    dom_weight = 1.0

                try:
                    ran_weight = odl_op.range.weighting.const
                except AttributeError:
                    ran_weight = 1.0

                scale = dom_weight / ran_weight
                grad_out *= scale
                # Tensorflow expects two gradients one for each input but we don't care for the second one (not defined anyway)
                # so let's put it to zero
                return grad_out, np.zeros(x.shape) 

            return out, _call_grad_impl

        def call(self, dy, x=None):
            if x is None:
                raise ValueError('x eval point needed')
            # if differentiable:
            #     result = self._call_impl(dy, x=x)
            # else:
            #     result = self._call_impl_nondiff(dy, x=x)
            if differentiable:
               result = tf.py_function(func=self._call_impl, inp = [dy, x], Tout=[tf.as_dtype(self.odtype)])
            else:
               result = tf.py_function(func=self._call_impl_nondiff, inp = [dy, x], Tout=[tf.as_dtype(self.odtype)])
            result = result[0]
            result = tf.convert_to_tensor(result)
            result.set_shape(self.oshape)
            return result

        def compute_output_shape(self, input_shape):
            # if self.fixed_size:
            ##    out_shape = self.oshape
            # else:
            #    out_shape = (input_shape[0],) + self.oshape[1:]
            return self.oshape

    return Odl_Op_Layer()


def space_shape(space):
    """Return ``space.shape``, including power space base shape.

    If ``space`` is a power space, return ``(len(space),) + space[0].shape``,
    otherwise return ``space.shape``.
    """
    if isinstance(space, odl.ProductSpace) and space.is_power_space:
        return (len(space),) + space[0].shape
    else:
        return space.shape


if __name__ == '__main__':
    from odl.util.testutils import run_doctests
    run_doctests()

# Copyright 2014-2018 The ODL contributors
#
# This file is part of ODL.
#
# This Source Code Form is subject to the terms of the Mozilla Public License,
# v. 2.0. If a copy of the MPL was not distributed with this file, You can
# obtain one at https://mozilla.org/MPL/2.0/.

"""ODL integration with shearlab."""

import odl
import numpy as np
from threading import Lock
import os
# Library for the shearlab library

#import julia
import matplotlib.pyplot as plt
from numpy import ceil
# from numpy.fft import fft2, ifft2, fftshift, ifftshift
import matplotlib.pyplot as plt
from itertools import compress
os.environ['PATH'] = "$PATH:/workspace/julia-1.4.2/bin"
from julia.api import LibJulia
api=LibJulia.load()
api.sysimage="/workspace/sys.so"
api.init_julia()
from julia import Main
from julia import Base
from julia import Shearlab
from julia import PyPlot
from julia import Images
import tensorflow as tf
from numpy.fft import fft2, ifft2, fftshift, ifftshift

__all__ = ('ShearlabOperator',)


class ShearlabOperator(odl.Operator):

    """Shearlet transform using Shearlab.jl as backend.
    This is the non-compact shearlet transform implemented using the Fourier
    transform.
    """

    def __init__(self, space, num_scales):
        """Initialize a new instance.

        Parameters
        ----------
        space : `DiscreteLp`
            The space on which the shearlet transform should act. Must be
            two-dimensional.
        num_scales : nonnegative `int`
            The number of scales for the shearlet transform, higher numbers
            mean better edge resolution but more computational burden.

        Examples
        --------
        Create a 2d-shearlet transform:
        >>> space = odl.uniform_discr([-1, -1], [1, 1], [128, 128])
        >>> shearlet_transform = ShearlabOperator(space, num_scales=2)
        """
        self.shearlet_system = getshearletsystem2D(
            space.shape[0], space.shape[1], num_scales)
        range = space ** self.shearlet_system.nShearlets
        self.mutex = Lock()
        super(ShearlabOperator, self).__init__(space, range, True)

    def _call(self, x):
        """``self(x)``."""
        with self.mutex:
            result = tfsheardec2D(x, self.shearlet_system)
            return np.moveaxis(result, -1, 0)

    @property
    def adjoint(self):
        """The adjoint operator."""
        op = self

        class ShearlabOperatorAdjoint(odl.Operator):

            """Adjoint of the shearlet transform.

            See Also
            --------
            odl.contrib.shearlab.ShearlabOperator
            """

            def __init__(self):
                """Initialize a new instance."""
                self.mutex = op.mutex
                self.shearlet_system = op.shearlet_system
                super(ShearlabOperatorAdjoint, self).__init__(
                    op.range, op.domain, True)

            def _call(self, x):
                """``self(x)``."""
                with op.mutex:
                    x = np.moveaxis(x, 0, -1)
                    return tfsheardecadjoint2D(x, op.shearlet_system)

            @property
            def adjoint(self):
                """The adjoint operator."""
                return op

            @property
            def inverse(self):
                """The inverse operator."""
                op = self

                class ShearlabOperatorAdjointInverse(odl.Operator):

                    """
                    Adjoint of the inverse/Inverse of the adjoint
                    of shearlet transform.

                    See Also
                    --------
                    odl.contrib.shearlab.ShearlabOperator
                    """

                    def __init__(self):
                        """Initialize a new instance."""
                        self.mutex = op.mutex
                        self.shearlet_system = op.shearlet_system
                        super(ShearlabOperatorAdjointInverse, self).__init__(
                            op.range, op.domain, True)

                    def _call(self, x):
                        """``self(x)``."""
                        with op.mutex:
                            result = tfshearrecadjoint2D(x, op.shearlet_system)
                            return np.moveaxis(result, -1, 0)

                    @property
                    def adjoint(self):
                        """The adjoint operator."""
                        return op.adjoint.inverse

                    @property
                    def inverse(self):
                        """The inverse operator."""
                        return op

                return ShearlabOperatorAdjointInverse()

        return ShearlabOperatorAdjoint()

    @property
    def inverse(self):
        """The inverse operator."""
        op = self

        class ShearlabOperatorInverse(odl.Operator):

            """Inverse of the shearlet transform.

            See Also
            --------
            odl.contrib.shearlab.ShearlabOperator
            """

            def __init__(self):
                """Initialize a new instance."""
                self.mutex = op.mutex
                self.shearlet_system = op.shearlet_system
                super(ShearlabOperatorInverse, self).__init__(
                    op.range, op.domain, True)

            def _call(self, x):
                """``self(x)``."""
                with op.mutex:
                    x = np.moveaxis(x, 0, -1)
                    return tfshearrec2D(x, op.shearlet_system)

            @property
            def adjoint(self):
                """The inverse operator."""
                op = self

                class ShearlabOperatorInverseAdjoint(odl.Operator):

                    """
                    Adjoint of the inverse/Inverse of the adjoint
                    of shearlet transform.

                    See Also
                    --------
                    odl.contrib.shearlab.ShearlabOperator
                    """

                    def __init__(self):
                        """Initialize a new instance."""

                        self.mutex = op.mutex
                        self.shearlet_system = op.shearlet_system
                        super(ShearlabOperatorInverseAdjoint, self).__init__(
                            op.range, op.domain, True)

                    def _call(self, x):
                        """``self(x)``."""
                        with op.mutex:
                            result = tfshearrecadjoint2D(x, op.shearlet_system)
                            return np.moveaxis(result, -1, 0)

                    @property
                    def adjoint(self):
                        """The adjoint operator."""
                        return op

                    @property
                    def inverse(self):
                        """The inverse operator."""
                        return op.inverse.adjoint

                return ShearlabOperatorInverseAdjoint()

            @property
            def inverse(self):
                """The inverse operator."""
                return op

        return ShearlabOperatorInverse()


# Python library for shearlab.jl

# def load_julia_with_Shearlab():
#     """Function to load Shearlab."""
#     # Importing base
#     from julia import Main
#     from julia import Base
#     from julia import Shearlab
#     from julia import PyPlot
#     from julia import Images
#     # j = julia.Julia()
#     # j.eval('using Shearlab')
#     # j.eval('using PyPlot')
#     # j.eval('using Images')
#     #return j


#load_julia_with_Shearlab()


def load_image(name, n, m=None, gpu=None, square=None):
    """Function to load images with certain size."""
    if m is None:
        m = n
    if gpu is None:
        gpu = 0
    if square is None:
        square = 0
    command = ('Shearlab.load_image("{}", {}, {}, {}, {})'.format(name,
               n, m, gpu, square))
    return Main.eval(command)


def imageplot(f, str=None, sbpt=None):
    """Plot an image generated by the library."""
    # Function to plot images
    if str is None:
        str = ''
    if sbpt is None:
        sbpt = []
    if sbpt != []:
        plt.subplot(sbpt[0], sbpt[1], sbpt[2])
    imgplot = plt.imshow(f, interpolation='nearest')
    imgplot.set_cmap('gray')
    plt.axis('off')
    if str != '':
        plt.title(str)


class Shearletsystem2D:
    """Class of shearlet system in 2D."""
    def __init__(self, shearlets, size, shearLevels, full, nShearlets,
                 shearletIdxs, dualFrameWeights, RMS, isComplex):
        self.shearlets = shearlets
        self.size = size
        self.shearLevels = shearLevels
        self.full = full
        self.nShearlets = nShearlets
        self.shearletIdxs = shearletIdxs
        self.dualFrameWeights = dualFrameWeights
        self.RMS = RMS
        self.isComplex = isComplex


def getshearletsystem2D(rows, cols, nScales, shearLevels=None,
                        full=None,
                        directionalFilter=None,
                        quadratureMirrorFilter=None):
    """Function to generate de 2D system."""
    if shearLevels is None:
        shearLevels = [float(ceil(i / 2)) for i in range(1, nScales + 1)]
    if full is None:
        full = 0
    if directionalFilter is None:
        directionalFilter = 'Shearlab.filt_gen("directional_shearlet")'
    if quadratureMirrorFilter is None:
        quadratureMirrorFilter = 'Shearlab.filt_gen("scaling_shearlet")'
    Main.eval('rows=' + str(rows))
    Main.eval('cols=' + str(cols))
    Main.eval('nScales=' + str(nScales))
    Main.eval('shearLevels=' + str(shearLevels))
    Main.eval('full=' + str(full))
    Main.eval('directionalFilter=' + directionalFilter)
    Main.eval('quadratureMirrorFilter=' + quadratureMirrorFilter)
    Main.eval('shearletsystem=Shearlab.getshearletsystem2D(rows, '
           'cols, nScales, shearLevels, full, directionalFilter, '
           'quadratureMirrorFilter) ')
    shearlets = Main.eval('shearletsystem.shearlets')
    size = Main.eval('shearletsystem.size')
    shearLevels = Main.eval('shearletsystem.shearLevels')
    full = Main.eval('shearletsystem.full')
    nShearlets = Main.eval('shearletsystem.nShearlets')
    shearletIdxs = Main.eval('shearletsystem.shearletIdxs')
    dualFrameWeights = Main.eval('shearletsystem.dualFrameWeights')
    RMS = Main.eval('shearletsystem.RMS')
    isComplex = Main.eval('shearletsystem.isComplex')
    Main.eval('shearletsystem = 0')
    return Shearletsystem2D(shearlets, size, shearLevels, full, nShearlets,
                            shearletIdxs, dualFrameWeights, RMS, isComplex)


# tfftshift and itfftshift

def tfftshift(xtf, axes=None):
    if len(xtf.shape)==3:
        ndim = len(xtf.shape)-1
    else:
        ndim = len(xtf.shape)-2
    if axes is None:
        axes = list(np.array(range(ndim))+1)
    elif isinstance(axes, integer_types):
        axes = (axes,)
    ytf = xtf
    for k in axes:
        n = int(ytf.shape[k])
        p2 = (n+1)//2
        mylist = np.concatenate((np.arange(p2, n), np.arange(p2)))
        ytf = tf.gather(ytf, indices = mylist, axis = k)
    return ytf

def itfftshift(ytf, axes=None):
    if len(ytf.shape)==3:
        ndim = len(ytf.shape)-1
    else:
        ndim = len(ytf.shape)-2
    if axes is None:
        axes = list(np.array(range(ndim))+1)
    elif isinstance(axes, integer_types):
        axes = (axes,)
    xtf = ytf
    for k in axes:
        n = int(xtf.shape[k])
        p2 = n-(n+1)//2
        mylist = np.concatenate((np.arange(p2, n), np.arange(p2)))
        xtf = tf.gather(xtf, indices = mylist, axis = k)
    return xtf

# shearlet decomposition
def tfsheardec2D(Xtf, tfshearlets):
    """Shearlet Decomposition function."""
    Xfreqtf = tf.signal.fftshift(tf.signal.fft2d(tf.signal.ifftshift(tf.cast(Xtf.asarray(),tf.complex128))))
    return tf.math.real(tf.signal.fftshift(tf.transpose(tf.signal.ifft2d(tf.transpose(tf.signal.ifftshift(tf.math.multiply(tf.expand_dims(Xfreqtf,2),
                                                                                        tf.math.conj(tfshearlets.shearlets)),axes=[0,1]),[2,0,1])),[1,2,0]),axes=[0,1]))

# shearlet reconstruction
def tfshearrec2D(coeffstf, tfshearlets ):
    Xfreqtf = tf.reduce_sum(tf.multiply(tf.signal.fftshift(tf.transpose(tf.signal.fft2d(tf.transpose(tf.signal.ifftshift(tf.cast(coeffstf,tf.complex128),axes=[0,1]),[2,0,1])),[1,2,0]),axes=[0,1]),
                                        tfshearlets.shearlets),axis=2)
    return tf.math.real(tf.signal.fftshift(tf.signal.ifft2d(tf.signal.ifftshift(tf.multiply(Xfreqtf,1/tfshearlets.dualFrameWeights)))))

# dual of decomposition
def tfsheardecadjoint2D(coeffstf, tfshearlets):
    Xfreqtf = tf.reduce_sum(tf.multiply(tf.signal.fftshift(tf.transpose(tf.signal.fft2d(tf.transpose(tf.signal.ifftshift(tf.cast(coeffstf,tf.complex128),axes=[0,1]),[2,0,1])),[1,2,0]),axes=[0,1]),
                                        tf.math.conj(tfshearlets.shearlets)),axis=2)
    return tf.math.real(tf.signal.fftshift(tf.signal.ifft2d(tf.signal.ifftshift(tf.multiply(Xfreqtf,1/tfshearlets.dualFrameWeights)))))

# dual of reconstruction
def tfshearrecadjoint2D(Xtf, tfshearlets):
    """Shearlet Decomposition function."""
    Xfreqtf = tf.signal.fftshift(tf.signal.fft2d(tf.signal.ifftshift(tf.cast(Xtf.asarray(),tf.complex128))))
    return tf.math.real(tf.signal.fftshift(tf.transpose(tf.signal.ifft2d(tf.transpose(tf.signal.ifftshift(tf.math.multiply(tf.expand_dims(Xfreqtf,2),
                                                                                        (tfshearlets.shearlets)),axes=[0,1]),[2,0,1])),[1,2,0]),axes=[0,1]))

def sheardec2D(X, shearletsystem):
    """Shearlet Decomposition function."""
    coeffs = np.zeros(shearletsystem.shearlets.shape, dtype=complex)
    Xfreq = fftshift(fft2(ifftshift(X)))
    for i in range(shearletsystem.nShearlets):
        coeffs[:, :, i] = fftshift(ifft2(ifftshift(Xfreq * np.conj(
                                   shearletsystem.shearlets[:, :, i]))))
    return coeffs.real


def shearrec2D(coeffs, shearletsystem):
    """Shearlet Recovery function."""
    X = np.zeros(coeffs.shape[:2], dtype=complex)
    for i in range(shearletsystem.nShearlets):
        X = X + fftshift(fft2(
            ifftshift(coeffs[:, :, i]))) * shearletsystem.shearlets[:, :, i]
    return (fftshift(ifft2(ifftshift((
            X / shearletsystem.dualFrameWeights))))).real


def sheardecadjoint2D(coeffs, shearletsystem):
    """Shearlet Decomposition adjoint function."""
    X = np.zeros(coeffs.shape[:2], dtype=complex)
    for i in range(shearletsystem.nShearlets):
        X = X + fftshift(fft2(
            ifftshift(coeffs[:, :, i]))) * np.conj(
            shearletsystem.shearlets[:, :, i])
    return (fftshift(ifft2(ifftshift(
            X / shearletsystem.dualFrameWeights)))).real


def shearrecadjoint2D(X, shearletsystem):
    """Shearlet Recovery adjoint function."""
    coeffs = np.zeros(shearletsystem.shearlets.shape, dtype=complex)
    Xfreq = fftshift(fft2(ifftshift(X)))
    for i in range(shearletsystem.nShearlets):
        coeffs[:, :, i] = fftshift(ifft2(ifftshift(
            Xfreq * shearletsystem.shearlets[:, :, i])))
    return coeffs.real

if __name__ == '__main__':
    from odl.util.testutils import run_doctests
    run_doctests()
